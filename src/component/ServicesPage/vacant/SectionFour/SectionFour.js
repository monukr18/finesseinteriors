import React, { Component } from 'react'
import '../SectionFour/SectionFour.css'
export default class SectionFour extends Component {
  render() {
    return (
      <div className="container mt-5 fonts ">
        <h4 className="txt-head">Flat rate starting at $2000 + HST</h4>
        <h4>Note:</h4>
        <ul>
          <li>The starting flat rate applies to properties that are less than 2750 sq ft.* (anything larger is priced on a house by house basis). Payment is due on the day of Staging.</li>
          <li>We will Stage the Master Suite and one Secondary Bedroom (as an Office). Note: Basements are not Staged.</li>
          <li>This price is to include furnishings for the Living Room or Family Room (not both). $500 + HST will be charged to Stage both.</li>
        <li>Initial Consultation (if deemed necessary- or pictures supplied by the Real Estate Agent/Seller), set up, tear down, transportation, manpower costs and insurance for up to 30 days or SOLD firm, whichever one comes first.</li>
        </ul>
      
      
     
      {/* <h2 className="txt-head">HERE IS HOW WE CAN HELP</h2>
      <h5 className="txt-italic pb-4">Our enthusiastic team will leave no stone untouched to finish your project on time.</h5>
      <p className="txt-para pb-4">Finesse Interiors Real Estate Staging is a billion dollar staging company. We will help you with decluttering things and remove all personal items so that buyers can imagine themselves in your home. Our staged home sells faster once they are on the market and for higher money. We will take care of everything from flooring to ceiling.</p>
      <p className="txt-para pb-4">We will provide details based on your requirement for your proposed home:</p>
      <ul>
      <li className="txt-para pb-4"><span className="bold">It Starts With An Appointment – </span>
      Once the appointment is fixed, we visit you for consultation and reviewing the property.</li>
      <li className="txt-para pb-4"><span className="bold">Tell Us What You Need – </span>Our staging consultants will either visit you at your home or you can meet with them in-store to discuss your thoughts and ideas.</li>
      <li className="txt-para pb-4"><span className="bold">We Do The ‘Legwork’ – </span>You can relax till we collect information and other necessary details for the projects to be completed.</li>
      <li className="txt-para pb-4"><span className="bold">Our Presentation To You – </span>Our consultant will arrange a suitable time with you to present the range selected for you to review and discuss the various options and ideas.</li>
      <li className="txt-para pb-4"><span className="bold">You Decide At Your Leisure – </span> You can take time to reach us back. We would love to hear from you at the earliest.
      What Happens Next? - Project stages are explained in detail. At every stage, we have discussion and review option if necessary.</li>
      <li className="txt-para pb-4"><span className="bold">What Happens Next? – </span>Distinctively exploit optimal alignments for intuitive bandwidth. Quickly coordinate e-business applications through revolutionary catalysts for change. Seamlessly underwhelm optimal testing procedures.</li>
      <li className="txt-para pb-4"><span className="bold">Give Us The Go Ahead – </span> Once it is approved, we start on the project immediately and your home will be ready to be sold in no time.</li>
      <li className="txt-para pt-4"><span className="bold">Your Beautiful Home – </span>Your beauty will be ready to hit the market.</li>
      </ul> */}
        
      </div>
    )
  }
}
