import React, { Component } from 'react'
import layout1 from '../../../images/Portfolio/The Gyatt House_03/Gyatt_House.png'
import layout4 from '../../../images/Home/layout4.jpg'
import layout3 from '../../../images/Home/layout3.png'
import layout5 from '../../../images/Home/layout5.jpg'
import HomeBackImage1 from '../../../images/hstage.jpg'
import HomeBrook from '../../../images/hstage.jpg'
import Tv from '../../../images/tv.png'
import nature from '../../../images/nature6.png'
import './HomeStageSectionFive.css'
export default class Sectionone extends Component {
  render() {
    return (
      <div className="bg-clr my_own_css">
      <div className="container pt-5" >
      <div className="row">

      
      </div>
      </div>
      <div className="container">
      <div className="row">
      
      <div className="col-md-6">
      
      <div class="content content_modi">
<a href="/Portfolio" target="_blank">
<div class="content-overlay"></div>
<img className="content-image" src={layout1}></img>
<div class="content-details content-details-modi fadeIn-top">
<div class="group">

<p className="pfont"></p>
<h4 className="textStyle">The Gyatt House</h4>

<span className=" spans pfont">View details</span>

</div>
</div>
</a>
</div>
      </div>
      <div className="col-md-6  ">
      <div class="content content_modi  content_modi_only">
<a href="/Portfolio" target="_blank">
<div class="content-overlay"></div>
<img className="content-image" src={HomeBackImage1}></img>
<div class="content-details fadeIn-top">
<div class="group">

<p className="pfont"></p>
<h4 className="textStyle">The Sagebrush Residence</h4>

<span className="spans pfont">View details</span>
</div>
</div>
</a>
</div>
<div class="content content_modi Content_modi_padding ">
<a href="/Portfolio" target="_blank">
<div class="content-overlay"></div>
<img className="content-image" src={layout3}></img>
<div class="content-details fadeIn-top">
<div class="group">

<p className="pfont"></p>
<h4 className="textStyle">Nature Haven</h4>

<span className="spans pfont">View details</span>

</div>
</div>
</a>
</div>
      </div>
      
      <div className="col-md-12">
      <div class="content content_modi Content_modi_padding">
<a href="/Portfolio" target="_blank">
<div class="content-overlay"></div>
<img className="content-image content-image1" src={layout4}></img>
<div class="content-details content-details-modi1 fadeIn-top">
<div class="group">

<p className="pfont"></p>
<h4 className="textStyle">The Savannah Residence</h4>

<span className="spans pfont">View details</span>

</div>
</div>
</a>
</div>
      </div>
      <div className="col-md-6">
      <div class="content content_modi Content_modi_padding">
<a href="/Portfolio" target="_blank">
<div class="content-overlay"></div>
<img className="content-image" src={HomeBrook}></img>
<div class="content-details fadeIn-top">
<div class="group">

<p className="pfont"></p>
<h4 className="textStyle">The Highbrooke House</h4>

<span className="spans pfont">View details</span>

</div>
</div>
</a>
</div>
      </div>
      <div className="col-md-6">
      <div class="content content_modi Content_modi_padding">
<a href="/Portfolio" target="_blank">
<div class="content-overlay"></div>
<img className="content-image" src={layout5}></img>
<div class="content-details fadeIn-top">
<div class="group">

<p className="pfont"></p>
<h4 className="textStyle">Wondrous Watford </h4>

<span className="spans pfont">View details</span>

</div>
</div>
</a>
</div>
      </div>
      </div>
      </div>
      </div>
    )
  }
}
